public class Point3d {

    private double x;
    private double y;
    private double z;

    public Point3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Point3d() {
        this (0, 0, 0);
    }

    public double getX() {
        return this.x;
    }
    public double getY() {
        return this.y;
    }
    public double getZ() {
        return this.z;
    }

    public void setX(double val) {
        this.x = val;
    }
    public void setY(double val) {
        this.y = val;
    }
    public void setZ(double val) {
        this.z = val;
    }

    public boolean equals(Object obj) {
        if (obj instanceof Point3d) {
            Point3d other = (Point3d) obj;
            return x == other.getX() &&
                    y == other.getY() &&
                    x == other.getZ();
        }

        return false;
    }

    public double distanceTo(Point3d other_point) {
        return Math.sqrt(Math.pow(x - other_point.getX(), 2) +
        Math.pow(y - other_point.getY(), 2) +
        Math.pow(z - other_point.getZ(), 2));
    }
}
