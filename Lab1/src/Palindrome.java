public class Palindrome {
    public static String reverseString(String str) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            result.insert(0, str.charAt(i));
        }
        return result.toString();
    }

    public static boolean isPalindrome(String str) {
        String reverseStr = reverseString(str);
        for (int i = 0; i < str.length(); i++){
            if (!(str.charAt(i) == reverseStr.charAt(i)))
                return false;
        }
        return true;
    }

    public static void main(String[] args) {
        for (String s : args) {
            if (isPalindrome((s)))
                System.out.printf("%s is palindrome\n", s);
        }
    }
}
