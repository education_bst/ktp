package src;

public class Location
{
    public int x;
    public int y;
    // Рандомные простые числа
    private final static int PRIME1 = 41;
    private final static int PRIME2 = 73;

    public Location(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public Location()
    {
        this(0, 0);
    }
    
    public boolean equals(Object obj) {
        
        if (obj instanceof Location) {
            
            Location other = (Location) obj;
            return x == other.x && y == other.y;
        }

        return false;
    }

    public int hashCode() {
        int result = PRIME1;

        result = PRIME2 * result + x;
        result = PRIME2 * result + y;
        return result;
    }
}
